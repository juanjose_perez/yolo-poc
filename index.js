/** @format */
import React from "react";
import { AppRegistry, NativeModules, Alert } from "react-native";
import { Provider, connect } from "react-redux";
import { Route, Switch, withRouter } from "react-router-native";
import { ConnectedRouter } from "connected-react-router";
import SideMenu from "react-native-side-menu";
import * as routes from "./src/layout/routes";
import MenuComponent from "./src/layout/components/MenuComponent";
import { name as appName } from "./app.json";
import store, { history } from "./src/state/store";
import { screenActions } from "./src/state/ducks/screen";

const { TensorFlowManager } = NativeModules;

TensorFlowManager.loadModel(error => {
  if (error) {
    Alert.alert("Error", JSON.stringify(error));
  }
});

const mapStateToProps = state => ({
  isOpen: state.screen.menu.isOpen
});

const mapDispatchToProps = dispatch => {
  return {
    onChange: isOpen => dispatch(screenActions.toggleMenu(isOpen))
  };
};

const ConnectedSideMenu = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SideMenu)
);

const RootComponent = () => {
  const menu = <MenuComponent />;
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <ConnectedSideMenu
          disableGestures={true}
          menu={menu}
          menuPosition="right"
        >
          <Switch>
            <Route exact path="/" component={routes.ImageProcessing} />
            <Route path="/training" component={routes.ClassTraining} />
            <Route path="/classSelector" component={routes.ClassSelector} />
            <Route
              path="/classifierSelector"
              component={routes.ClassifierSelector}
            />
          </Switch>
        </ConnectedSideMenu>
      </ConnectedRouter>
    </Provider>
  );
};

AppRegistry.registerComponent(appName, () => RootComponent);
