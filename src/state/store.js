import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import { Dimensions } from "react-native";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas";
import * as reducers from "./ducks";
import { connectRouter } from "connected-react-router";
import { routerMiddleware } from "connected-react-router";
import { createMemoryHistory } from "history";

/* State shape
{
  dimensions:{height: number, width: number}
}
*/
export const history = createMemoryHistory();
const sagaMiddleware = createSagaMiddleware();
const configureStore = initialState => {
  const rootReducer = combineReducers({
    ...reducers,
    router: connectRouter(history)
  });
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware, routerMiddleware(history)))
  );
  sagaMiddleware.run(rootSaga);
  return store;
};

const initialState = {
  classifier: {
    list: [{ id: 0, name: "None" }],
    current: 0,
    downloading: false
  },
  image: {
    processedImage: {
      dimensions: { height: 0, width: 0 },
      rects: [],
      imageURI: undefined,
      showBox: true,
      evaluating: false
    },
    trainingImages: {
      uris: [],
      folder: "",
      processing: false
    }
  },
  screen: { dimensions: Dimensions.get("window"), menu: { isOpen: false } },
  "class": []
};

export default configureStore(initialState);
