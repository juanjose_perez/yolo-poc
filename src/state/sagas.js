import { imageSaga } from "./ducks/image";
import { screenSaga } from "./ducks/screen";
import { classSaga } from "./ducks/class";
import { classifierSaga } from "./ducks/classifier";
import { all, call } from "redux-saga/effects";

export default function* rootSaga() {
  yield all([
    call(imageSaga),
    call(screenSaga),
    call(classSaga),
    call(classifierSaga)
  ]);
}
