import { call, take, put, all, select } from "redux-saga/effects";
import RNFS from "react-native-fs";
import { Alert } from "react-native";
import axios from "axios";
import { getClassifierName, getClassifierDate } from "./selectors";
import * as classifierTypes from "./types";

const BASE_URL = "https://nachorubio.pythonanywhere.com";
const HEADERS = {
  Authorization: "Token 002ab350ec89d581a2c40abaea284db7e997c1de"
};

const caller = axios.create({
  baseURL: BASE_URL,
  headers: HEADERS
});

function* getClassifiersSaga() {
  while (true) {
    yield take(classifierTypes.GET_LIST);
    const response = yield call(caller.get, "classifiers/");
    const classifiers = response.data;
    classifiers.unshift({ id: 0, name: "None" });
    yield put({
      type: classifierTypes.GET_LIST_SUCCESS,
      payload: { list: classifiers }
    });
  }
}

function* selectClassifierSaga() {
  const convertToBase64 = fileContent => {
    return new Promise(resolve => {
      const reader = new FileReader();
      reader.readAsDataURL(fileContent);
      reader.onloadend = () => {
        const base64data = reader.result;
        resolve(base64data.split(",").pop());
      };
    });
  };

  while (true) {
    const action = yield take(classifierTypes.SELECT);
    try {
      const id = action.payload;
      if (id !== 0) {
        const classifierName = yield select(getClassifierName);
        const classifierDate = yield select(getClassifierDate);
        const filename = `${RNFS.DocumentDirectoryPath}/${classifierName}`;
        const bExists = yield RNFS.exists(filename);
        let bIsRecent = true;
        if (bExists) {
          const fileStat = yield RNFS.stat(filename);
          if (fileStat.mtime.getTime() < classifierDate.getTime()) {
            bIsRecent = false;
          }
        }

        if (!bExists || !bIsRecent) {
          const fileContent = yield call(caller.get, `classifiers/${id}`, {
            responseType: "blob"
          });
          const base64data = yield call(convertToBase64, fileContent.data);
          yield call(RNFS.writeFile, filename, base64data, "base64");
        }
      }

      yield put({ type: classifierTypes.SELECT_SUCCESS });
    } catch (ex) {
      Alert.alert("Error", JSON.stringify(ex));
      yield put({ type: classifierTypes.SELECT_ERROR });
    }
  }
}

export default function* rootSaga() {
  yield all([call(getClassifiersSaga), call(selectClassifierSaga)]);
}
