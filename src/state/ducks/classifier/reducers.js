import * as classifierTypes from "./types";
import { createReducer } from "../../utils";

/* State shape
{
  dimensions: { height: number, width: number }, imageURI: string, rects: [], evaluating: boolean, showBox: boolean}
}
*/

const initialState = [];

export default createReducer(initialState)({
  [classifierTypes.GET_LIST]: state => {
    return {
      ...state,
      downloading: true
    };
  },
  [classifierTypes.GET_LIST_SUCCESS]: (state, action) => {
    return {
      ...state,
      downloading: false,
      list: action.payload.list
    };
  },
  [classifierTypes.SELECT]: (state, action) => {
    return {
      ...state,
      downloading: true,
      current: action.payload
    };
  },
  [classifierTypes.SELECT_SUCCESS]: (state, action) => {
    return {
      ...state,
      downloading: false
    };
  },
  [classifierTypes.SELECT_ERROR]: (state, action) => {
    return {
      ...state,
      downloading: false,
      current: 0
    };
  }
});
