import reducer from "./reducers";

import * as classifierActions from "./actions";
import classifierSaga from "./sagas";
//import * as screenSelectors from "./selectors";

export { classifierActions, classifierSaga /*, screenSelectors*/ };

export default reducer;
