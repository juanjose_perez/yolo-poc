import * as classifierTypes from "./types";

export const getClassifierList = () => ({
  type: classifierTypes.GET_LIST
});

export const selectClassifier = id => ({
  type: classifierTypes.SELECT,
  payload: id
});
