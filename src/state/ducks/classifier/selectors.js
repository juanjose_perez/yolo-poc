export const getClassifierList = state => state.classifier.list;
export const getClassifierName = state => {
  if (!state.classifier.current) {
    return null;
  }
  return `${state.classifier.list[state.classifier.current].name}.tflite`;
};

export const getClassifierDate = state => {
  if (!state.classifier.current) {
    return null;
  }
  return new Date(state.classifier.list[state.classifier.current].date);
};
