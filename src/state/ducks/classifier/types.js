export const GET_LIST = "classifier/GET_LIST";
export const GET_LIST_SUCCESS = "classifier/GET_LIST_SUCCESS";
export const SELECT = "classifier/SELECT";
export const SELECT_SUCCESS = "classifier/SELECT_SUCCESS";
export const SELECT_ERROR = "classifier/SELECT_ERROR";
