import * as screenTypes from "./types";
import * as imageTypes from "../image/types";
import { take, put, all, call } from "redux-saga/effects";

export function* updateDimensionsSaga() {
  while (true) {
    yield take(screenTypes.UPDATE_DIMENSIONS);
    yield put({ type: imageTypes.RESET_IMAGE });
  }
}

export default function* rootSaga() {
  yield all([call(updateDimensionsSaga)]);
}
