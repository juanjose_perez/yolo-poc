import * as screenTypes from "./types";

export const resizeScreen = event => ({
  type: screenTypes.UPDATE_DIMENSIONS,
  payload: event.nativeEvent.layout
});

export const toggleMenu = open => ({
  type: screenTypes.TOGGLE_MENU,
  payload: { open }
});
