import * as screenTypes from "./types";
import { createReducer } from "../../utils";

/* State shape
{
  dimensions:{height: number, width: number}, menu: {isOpen: boolean}
}
*/

const initialState = [];

const reducer = createReducer(initialState)({
  [screenTypes.UPDATE_DIMENSIONS]: (state, action) => {
    return { ...state, dimensions: action.payload };
  },
  [screenTypes.TOGGLE_MENU]: (state, action) => {
    return { ...state, menu: { isOpen: action.payload.open } };
  }
});

export default reducer;
