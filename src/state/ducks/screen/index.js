import reducer from "./reducers";

import * as screenActions from "./actions";
import screenSaga from "./sagas";
//import * as screenSelectors from "./selectors";

export { screenActions, screenSaga /*, screenSelectors*/ };

export default reducer;
