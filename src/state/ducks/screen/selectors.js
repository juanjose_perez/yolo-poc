export const getScreenDimensions = state => state.screen.dimensions;
export const getMenuOpen = state => state.screen.menu.isOpen;
