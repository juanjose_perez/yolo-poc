import * as imageTypes from "./types";

export const resetImage = () => ({
  type: imageTypes.RESET_IMAGE
});

export const takeImage = event => ({
  type: imageTypes.TAKE_IMAGE,
  payload: event
});

export const handlePredictions = event => ({
  type: imageTypes.HANDLE_PREDICTIONS,
  payload: event
});

export const toggleShowbox = () => ({
  type: imageTypes.TOGGLE_SHOWBOX
});

export const startTraining = () => ({
  type: imageTypes.TRAINING_IMAGE_START
});

export const startBurstTrainingImage = () => ({
  type: imageTypes.TRAINING_IMAGE_START_BURST
});

export const stopBurstTrainingImage = () => ({
  type: imageTypes.TRAINING_IMAGE_STOP_BURST
});

export const stopTraining = () => ({
  type: imageTypes.TRAINING_IMAGE_STOP
});
