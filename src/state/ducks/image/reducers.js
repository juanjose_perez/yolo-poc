import * as imageTypes from "./types";
import { createReducer } from "../../utils";

/* State shape
{
  dimensions: { height: number, width: number }, imageURI: string, rects: [], evaluating: boolean, showBox: boolean}
}
*/

const initialState = [];

const reducer = createReducer(initialState)({
  [imageTypes.RESET_IMAGE]: state => {
    return {
      ...state,
      processedImage: {
        dimensions: { heigbt: 0, width: 0 },
        imageURI: null,
        rects: [],
        evaluating: false,
        showBox: true
      }
    };
  },
  [imageTypes.TAKE_IMAGE]: state => {
    return {
      ...state,
      processedImage: {
        ...state.processedImage,
        evaluating: true
      }
    };
  },
  [imageTypes.TAKE_IMAGE_COMPLETE]: (state, action) => {
    return {
      ...state,
      processedImage: {
        ...state.processedImage,
        rects: [],
        dimensions: action.payload.dimensions,
        imageURI: action.payload.imageURI
      }
    };
  },
  [imageTypes.HANDLE_PREDICTIONS]: (state, action) => {
    return {
      ...state,
      processedImage: {
        ...state.processedImage,
        evaluating: false,
        rects: action.payload.rects,
        showBox: true
      }
    };
  },
  [imageTypes.TOGGLE_SHOWBOX]: state => {
    return {
      ...state,
      processedImage: {
        ...state.processedImage,
        showBox: !state.showBox
      }
    };
  },
  [imageTypes.TRAINING_IMAGE_SET_FOLDER]: (state, action) => {
    return {
      ...state,
      trainingImages: {
        ...state.trainingImages,
        folder: action.payload.folder
      }
    };
  },
  [imageTypes.TRAINING_IMAGE_ADD]: (state, action) => {
    return {
      ...state,
      trainingImages: {
        ...state.trainingImages,
        uris: [...state.trainingImages.uris, action.payload.uri]
      }
    };
  },
  [imageTypes.TRAINING_IMAGE_STOP]: (state, action) => {
    return {
      ...state,
      trainingImages: {
        ...state.trainingImages,
        processing: true
      }
    };
  },
  [imageTypes.TRAINING_IMAGE_END]: (state, action) => {
    return {
      ...state,
      trainingImages: {
        uris: [],
        folder: "",
        processing: false
      }
    };
  }
});

export default reducer;
