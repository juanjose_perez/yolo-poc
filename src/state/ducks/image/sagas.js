import { IPCamera } from "../../../layout/routes/ImageProcessing";
import { CTCamera } from "../../../layout/routes/ClassTraining";
import * as imageTypes from "./types";
import {
  getImageDimensions,
  getTrainingFolder,
  getNumberOfTakenPhotos
} from "./selectors";
import { getScreenDimensions } from "../screen/selectors";
import { getClasses } from "../class/selectors";
import RNFS from "react-native-fs";
import { zip } from "react-native-zip-archive";
import { Alert, ToastAndroid } from "react-native";
import { eventChannel } from "redux-saga";
import {
  call,
  all,
  select,
  take,
  put,
  spawn,
  fork,
  cancel
} from "redux-saga/effects";

import {
  NativeModules,
  NativeEventEmitter,
  DeviceEventEmitter,
  Platform
} from "react-native";
const { TensorFlowManager } = NativeModules;
const TensorFlowEmitter = new NativeEventEmitter(TensorFlowManager);
const yoloH = 416,
  yoloW = 416;

function* takePictureSaga() {
  const fnAlertHandle = error => {
    Alert.alert("Error", JSON.stringify(error));
  };
  while (true) {
    yield take(imageTypes.TAKE_IMAGE);
    const options = {
      quality: 1,
      fixOrientation: true,
      forceUpOrientation: false
    };
    const data = yield IPCamera.takePictureAsync(options);
    const screenDimensions = yield select(getScreenDimensions);
    const scale = {
      height: data.height / screenDimensions.height,
      width: data.width / screenDimensions.width
    };
    let dimensions;
    if (scale.height / scale.width > 1) {
      dimensions = {
        width: screenDimensions.width,
        height: (screenDimensions.height * scale.height) / scale.width
      };
    } else {
      dimensions = {
        width: (screenDimensions.width * scale.width) / scale.height,
        height: screenDimensions.height
      };
    }
    yield put({
      type: imageTypes.TAKE_IMAGE_COMPLETE,
      payload: {
        imageURI: data.uri,
        dimensions
      }
    });
    yield call(TensorFlowManager.recognizeImage, data.uri, fnAlertHandle);
  }
}

function predictionListener() {
  return eventChannel(emitter => {
    let subscription;
    if (Platform.OS === "ios") {
      subscription = TensorFlowEmitter.addListener("predictions", emitter);
    } else {
      subscription = DeviceEventEmitter.addListener("predictions", emitter);
    }

    return subscription.remove;
  });
}

function* handlePredictionsSaga() {
  const chan = yield call(predictionListener);
  while (true) {
    const results = yield take(chan);
    const dimensions = yield select(getImageDimensions);
    const classes = yield select(getClasses);

    if (results && results.length > 0) {
      results.sort((a, b) => {
        return a.confidenceInClass < b.confidenceInClass;
      });

      let rects = [
        {
          ...results[0].rect,
          confidence: results[0].confidenceInClass,
          detectedClass: results[0].detectedClass,
          detectedClassIndex: results[0].detectedClassIndex
        }
      ];

      let cnt = 0;
      for (let i = 1; i < results.length; i++) {
        const a = results[i].rect;
        let add = true;

        for (let j = 0; j <= cnt; j++) {
          const b = rects[j];

          const XA1 = a.x;
          const XA2 = a.x + a.w;
          const XB1 = b.x;
          const XB2 = b.x + b.w;
          const YA1 = a.y;
          const YA2 = a.y + a.h;
          const YB1 = b.y;
          const YB2 = b.y + b.h;

          const SA = a.w * a.h;
          const SB = b.w * b.h;
          const SI =
            Math.max(0, Math.min(XA2, XB2) - Math.max(XA1, XB1)) *
            Math.max(0, Math.min(YA2, YB2) - Math.max(YA1, YB1));
          const SU = SA > SB ? SB : SA;

          const ratio = SI / SU;
          if (ratio > 0.5) {
            add = false;
          }
        }

        if (add) {
          cnt++;
          rects.push({
            ...results[i].rect,
            confidence: results[i].confidenceInClass,
            detectedClass: results[i].detectedClass,
            detectedClassIndex: results[i].detectedClassIndex
          });
        }
      }

      const ratioH = dimensions.height / yoloH;
      const ratioW = dimensions.width / yoloW;

      rects = rects.map(rect => {
        const newRect = {
          x: rect.x * ratioW,
          y: rect.y * ratioH,
          width: rect.w * ratioW,
          height: rect.h * ratioH,
          confidence: rect.confidence,
          detectedClass: rect.detectedClass,
          detectedClassIndex: rect.detectedClassIndex
        };
        return newRect;
      });

      // filter by class

      rects = rects.filter(rect => {
        const currentClass = classes[rect.detectedClassIndex];
        return currentClass.active;
      });

      yield put({ type: imageTypes.HANDLE_PREDICTIONS, payload: { rects } });
    } else {
      yield put({
        type: imageTypes.HANDLE_PREDICTIONS,
        payload: { rects: [] }
      });
    }
  }
}

function* startTrainingSaga() {
  while (true) {
    yield take(imageTypes.TRAINING_IMAGE_START);
    const sFolderName = `${RNFS.CachesDirectoryPath}`;
    yield call(RNFS.mkdir, sFolderName);
    yield put({
      type: imageTypes.TRAINING_IMAGE_SET_FOLDER,
      payload: { folder: sFolderName }
    });
  }
}

function* burstPhotoSaga() {
  const takeBurstPhotoSaga = function*() {
    const asyncTakeBurstPhotoSaga = function*() {
      while (true) {
        const options = {
          quality: 0.05,
          skipProcessing: true
        };
        const data = yield CTCamera.takePictureAsync(options);

        yield put({
          type: imageTypes.TRAINING_IMAGE_ADD,
          payload: { uri: data.uri }
        });
      }
    };

    while (true) {
      yield take(imageTypes.TRAINING_IMAGE_START_BURST);
      const takePhotoSaga = yield fork(asyncTakeBurstPhotoSaga);
      yield take(imageTypes.TRAINING_IMAGE_STOP_BURST);
      yield cancel(takePhotoSaga);
    }
  };

  const photoAlertSaga = function*() {
    let lastTask;

    const debouncedPhotoAlertSaga = function*() {
      const numberPhotos = yield select(getNumberOfTakenPhotos);
      yield call(
        ToastAndroid.showWithGravity,
        `You have taken ${numberPhotos} photos`,
        ToastAndroid.SHORT,
        ToastAndroid.TOP
      );
    };

    while (true) {
      yield take(imageTypes.TRAINING_IMAGE_ADD);
      if (lastTask) {
        yield cancel(lastTask);
      }
      lastTask = yield fork(debouncedPhotoAlertSaga);
    }
  };

  yield spawn(photoAlertSaga);
  yield spawn(takeBurstPhotoSaga);
}

function* stopTrainingSaga() {
  while (true) {
    yield take(imageTypes.TRAINING_IMAGE_STOP);
    const folder = yield select(getTrainingFolder);
    const path = yield call(
      zip,
      folder,
      `${RNFS.ExternalCachesDirectoryPath}/training_${Date.now()}.zip`
    );
    yield call(Alert.alert, "Zip has been saved", path);
    yield put({ type: imageTypes.TRAINING_IMAGE_END });
    yield call(RNFS.unlink, folder);
  }
}

export default function* rootSaga() {
  yield all([
    call(takePictureSaga),
    call(handlePredictionsSaga),
    call(startTrainingSaga),
    call(burstPhotoSaga),
    call(stopTrainingSaga)
  ]);
}
