export const getImageDimensions = state =>
  state.image.processedImage.dimensions;

export const getTrainingFolder = state => state.image.trainingImages.folder;

export const getNumberOfTakenPhotos = state =>
  state.image.trainingImages.uris.length;
