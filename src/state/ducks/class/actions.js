import * as classTypes from "./types";

export const addClass = name => ({
  type: classTypes.ADD_CLASS,
  payload: { name }
});

export const toggleClass = name => ({
  type: classTypes.TOGGLE_CLASS,
  payload: { name }
});
