import * as classTypes from "./types";
import { put, all, call } from "redux-saga/effects";
import RNFS from "react-native-fs";

export function* initialReadClasses() {
  const fileContent = yield call(
    RNFS.readFileAssets,
    "tiny-yolo-voc-labels.txt"
  );
  const classStringArray = fileContent.replace(/\r/g, "").split("\n");
  for (let index = 0; index < classStringArray.length; index++) {
    yield put({
      type: classTypes.ADD_CLASS,
      payload: { name: classStringArray[index] }
    });
  }
}

export default function* rootSaga() {
  yield all([call(initialReadClasses)]);
}
