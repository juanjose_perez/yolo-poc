import * as classTypes from "./types";
import { createReducer } from "../../utils";

/* State shape
{
  class:[name: number, width: number]
}
*/

const initialState = [];

const reducer = createReducer(initialState)({
  [classTypes.ADD_CLASS]: (state, action) => {
    return [...state, { name: action.payload.name, active: true }];
  },
  [classTypes.TOGGLE_CLASS]: (state, action) => {
    const index = state.findIndex(
      imageClass => imageClass.name === action.payload.name
    );
    return [
      ...state.slice(0, index),
      { name: action.payload.name, active: !state[index].active },
      ...state.slice(index + 1)
    ];
  }
});

export default reducer;
