/** @format */

import React, { Component } from "react";
import { ActivityIndicator, StyleSheet, ScrollView, View } from "react-native";
import Svg, { Text } from "react-native-svg";
import { Header, ListItem } from "react-native-elements";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { classifierActions } from "../../../state/ducks/classifier";
import { screenActions } from "../../../state/ducks/screen";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white"
  },
  listContainer: {
    marginTop: 0
  },
  headerStylePortrait: {
    paddingTop: 0,
    width: "100%",
    height: "7.5%",
    borderBottomColor: "transparent"
  },
  headerStyleLandscape: {
    paddingTop: 0,
    width: "100%",
    height: "12%",
    borderBottomColor: "transparent"
  },
  evaluating: {
    flex: 1,
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  }
});

class ClassSelector extends Component {
  getHeaderStyle() {
    if (
      this.props.screen.dimensions.width > this.props.screen.dimensions.height
    ) {
      return styles.headerStyleLandscape;
    }
    return styles.headerStylePortrait;
  }

  componentDidMount() {
    this.props.getClassifierList();
  }

  render() {
    return this.props.classifier.downloading ? (
      <View style={styles.evaluating}>
        <ActivityIndicator size="large" color="#0000ff" animating={true} />
        <Svg
          width={this.props.screen.dimensions.width}
          height={this.props.screen.dimensions.height / 3}
        >
          <Text
            fill="black"
            stroke="black"
            fontSize="30"
            x={this.props.screen.dimensions.width / 2}
            y={this.props.screen.dimensions.height / 6}
            textAnchor="middle"
          >
            Processing
          </Text>
        </Svg>
      </View>
    ) : (
      <View
        style={styles.container}
        onLayout={event => {
          this.props.resizeScreen(event);
          this.props.toggleMenu(false);
        }}
      >
        <Header
          containerStyle={this.getHeaderStyle()}
          centerComponent={{
            text: "Select classifier",
            style: { color: "#fff" }
          }}
          rightComponent={{
            icon: "menu",
            color: "#fff",
            onPress: () => this.props.toggleMenu(true),
            underlayColor: "#64b5f6"
          }}
        />
        <ScrollView style={styles.listContainer}>
          {this.props.classifier.list.map((classObject, classIndex) => (
            <ListItem
              chevron={false}
              key={classIndex}
              title={classObject.name}
              leftIcon={{
                name:
                  classIndex === this.props.classifier.current
                    ? "radio-button-checked"
                    : "radio-button-unchecked",
                color:
                  classIndex === this.props.classifier.current
                    ? "#1e96f3"
                    : "#eee"
              }}
              onPress={() => {
                this.props.selectClassifier(classIndex);
              }}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}

ClassSelector.propTypes = {
  screen: PropTypes.object,
  classifier: PropTypes.object,
  resizeScreen: PropTypes.func,
  toggleMenu: PropTypes.func,
  getClassifierList: PropTypes.func,
  selectClassifier: PropTypes.func
};

const mapStateToProps = state => ({
  screen: state.screen,
  classifier: state.classifier
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { ...screenActions, ...classifierActions },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClassSelector);
