import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Image } from "react-native";

import BndBox from "./BndBox";
import ClearButton from "./ClearButton";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    backgroundColor: "white"
  }
});

const ProcessedImage = props => {
  return (
    <View style={styles.container}>
      <View
        style={[
          styles.container,
          { justifyContent: "center", alignItems: "center" }
        ]}
      >
        <Image
          source={{ uri: props.imageURI }}
          style={{
            width: props.dimensions.width,
            height: props.dimensions.height
          }}
        />
      </View>
      <BndBox
        rects={props.rects}
        dimensions={props.dimensions}
        showBox={props.showBox}
        toggleShowbox={props.toggleShowbox}
      />
      <ClearButton onClear={props.onClear} />
    </View>
  );
};

ProcessedImage.propTypes = {
  imageURI: PropTypes.string,
  dimensions: PropTypes.object,
  showBox: PropTypes.bool,
  toggleShowbox: PropTypes.func,
  onClear: PropTypes.func,
  rects: PropTypes.array
};

export default ProcessedImage;
