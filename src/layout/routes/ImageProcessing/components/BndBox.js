import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, TouchableOpacity } from "react-native";

import Svg, { Rect, Text, G } from "react-native-svg";

const colorArray = [
  "#e6194b",
  "#3cb44b",
  "#ffe119",
  "#4363d8",
  "#f58231",
  "#911eb4",
  "#46f0f0",
  "#f032e6",
  "#bcf60c",
  "#fabebe",
  "#008080",
  "#e6beff",
  "#9a6324",
  "#fffac8",
  "#800000",
  "#aaffc3",
  "#808000",
  "#ffd8b1",
  "#000075",
  "#808080",
  "#ffffff",
  "#000000"
];

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center"
  }
});

const BndBox = props => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.toggleShowbox}>
      <Svg height={props.dimensions.height} width={props.dimensions.width}>
        {props.showBox && props.rects && props.rects.length > 0
          ? props.rects.map((rect, index) => {
              return (
                <G key={index}>
                  <Rect
                    x={rect.x}
                    y={rect.y}
                    width={rect.width}
                    height={rect.height}
                    stroke={colorArray[rect.detectedClassIndex]}
                    strokeWidth="5"
                    fill="none"
                  />
                  <Text
                    fill={colorArray[rect.detectedClassIndex]}
                    fontSize="18"
                    fontWeight="bold"
                    x={rect.x + 10}
                    y={rect.y + 20}
                    textAnchor="start"
                  >
                    {(rect.confidence * 100).toFixed(2)}%
                  </Text>
                  <Text
                    fill={colorArray[rect.detectedClassIndex]}
                    fontSize="18"
                    fontWeight="bold"
                    x={rect.x + 10}
                    y={rect.y + rect.height - 20}
                    textAnchor="start"
                  >
                    {rect.detectedClass}
                  </Text>
                </G>
              );
            })
          : undefined}
      </Svg>
    </TouchableOpacity>
  );
};

BndBox.propTypes = {
  dimensions: PropTypes.object,
  showBox: PropTypes.bool,
  toggleShowbox: PropTypes.func,
  onClear: PropTypes.func,
  rects: PropTypes.array
};

export default BndBox;
