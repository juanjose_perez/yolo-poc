import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, TouchableOpacity } from "react-native";

import Svg, { Text as SvgText } from "react-native-svg";

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    backgroundColor: "transparent",
    alignSelf: "center",
    bottom: 0
  },
  clearButton: {
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  }
});

const ClearButton = ({ onClear }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.clearButton} onPress={onClear}>
        <Svg height="30" width="100">
          <SvgText
            fill="white"
            stroke="black"
            strokeWidth="0.6"
            fontWeight="bold"
            fontSize="18"
            x="50"
            y="20"
            textAnchor="middle"
          >
            Go Back
          </SvgText>
        </Svg>
      </TouchableOpacity>
    </View>
  );
};

ClearButton.propTypes = {
  onClear: PropTypes.func
};

export default ClearButton;
