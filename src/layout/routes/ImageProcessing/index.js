/** @format */

import React, { Component } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  View,
  TouchableOpacity
} from "react-native";
import { Header } from "react-native-elements";
import Svg, { Text } from "react-native-svg";
import { RNCamera } from "react-native-camera";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { screenActions } from "../../../state/ducks/screen";
import { imageActions } from "../../../state/ducks/image";

import ProcessedImage from "./components/ProcessedImage";

let IPCamera;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white"
  },
  camera: {
    flex: 1,
    backgroundColor: "transparent",
    height: "100%",
    width: "100%"
  },
  cameraContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    height: "100%",
    width: "100%"
  },
  button: {
    flex: 0,
    backgroundColor: "grey",
    borderRadius: 40,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: "center",
    margin: 20,
    height: 70,
    width: 70
  },
  headerContainer: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start"
  },
  headerStylePortrait: {
    paddingTop: 0,
    width: "100%",
    height: "7%",
    borderBottomColor: "transparent"
  },
  headerStyleLandscape: {
    paddingTop: 0,
    width: "100%",
    height: "12.5%",
    borderBottomColor: "transparent"
  },
  menuIcon: {
    color: "grey",
    backgroundColor: "transparent",
    margin: 10
  },
  evaluating: {
    flex: 1,
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  }
});

class ImageProcessing extends Component {
  getHeaderStyle() {
    if (
      this.props.screen.dimensions.width > this.props.screen.dimensions.height
    ) {
      return styles.headerStyleLandscape;
    }
    return styles.headerStylePortrait;
  }

  render() {
    return (
      <View
        style={styles.container}
        onLayout={event => {
          this.props.resizeScreen(event);
          this.props.toggleMenu(false);
        }}
      >
        <RNCamera
          ref={ref => (IPCamera = ref)}
          captureAudio={false}
          style={styles.camera}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.off}
          permissionDialogTitle={"Permission to use camera"}
          permissionDialogMessage={
            "We need your permission to use your camera phone"
          }
        >
          {this.props.image.processedImage.evaluating ? (
            <View style={styles.evaluating}>
              <ActivityIndicator
                size="large"
                color="#0000ff"
                animating={true}
              />
              <Svg
                width={this.props.screen.dimensions.width}
                height={this.props.screen.dimensions.height / 3}
              >
                <Text
                  fill="black"
                  stroke="black"
                  fontSize="30"
                  x={this.props.screen.dimensions.width / 2}
                  y={this.props.screen.dimensions.height / 6}
                  textAnchor="middle"
                >
                  Evaluating
                </Text>
              </Svg>
            </View>
          ) : (
            <View style={styles.cameraContainer}>
              <Header
                containerStyle={this.getHeaderStyle()}
                centerComponent={{
                  text: "Image Classification",
                  style: { color: "#fff", marginTop: 0 }
                }}
                rightComponent={{
                  icon: "menu",
                  color: "#fff",
                  onPress: () => this.props.toggleMenu(true),
                  underlayColor: "#64b5f6"
                }}
              />
              <TouchableOpacity
                onPress={this.props.takeImage}
                style={
                  this.props.image.processedImage.evaluating
                    ? {}
                    : styles.button
                }
              />
            </View>
          )}
        </RNCamera>
        {this.props.image.processedImage.imageURI &&
        !this.props.image.processedImage.evaluating ? (
          <ProcessedImage
            imageURI={this.props.image.processedImage.imageURI}
            rects={this.props.image.processedImage.rects}
            screen={this.props.screen}
            dimensions={this.props.image.processedImage.dimensions}
            showBox={this.props.image.processedImage.showBox}
            onClear={this.props.resetImage}
            toggleShowbox={this.props.toggleShowbox}
          />
        ) : (
          undefined
        )}
      </View>
    );
  }
}

ImageProcessing.propTypes = {
  screen: PropTypes.object,
  image: PropTypes.object,
  toggleShowbox: PropTypes.func,
  takeImage: PropTypes.func,
  resizeScreen: PropTypes.func,
  resetImage: PropTypes.func,
  toggleMenu: PropTypes.func
};

const mapStateToProps = state => ({
  screen: state.screen,
  image: state.image
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ ...screenActions, ...imageActions }, dispatch);
};

export { IPCamera };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ImageProcessing);
