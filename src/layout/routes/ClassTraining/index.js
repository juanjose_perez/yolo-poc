/** @format */

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import Svg, { Text } from "react-native-svg";
import { Header } from "react-native-elements";
import { RNCamera } from "react-native-camera";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { screenActions } from "../../../state/ducks/screen";
import { imageActions } from "../../../state/ducks/image";

let CTCamera;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white"
  },
  camera: {
    flex: 1,
    backgroundColor: "transparent",
    height: "100%",
    width: "100%"
  },
  cameraContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    height: "100%",
    width: "100%"
  },
  button: {
    flex: 0,
    backgroundColor: "grey",
    borderRadius: 40,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: "center",
    margin: 20,
    height: 70,
    width: 70
  },
  headerContainer: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  headerStylePortrait: {
    paddingTop: 0,
    width: "100%",
    height: "7.5%",
    borderBottomColor: "transparent"
  },
  headerStyleLandscape: {
    paddingTop: 0,
    width: "100%",
    height: "12%",
    borderBottomColor: "transparent"
  },
  menuIcon: {
    color: "grey",
    backgroundColor: "transparent",
    margin: 10
  },
  evaluating: {
    flex: 1,
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  }
});

class ClassTraining extends Component {
  getHeaderStyle() {
    if (
      this.props.screen.dimensions.width > this.props.screen.dimensions.height
    ) {
      return styles.headerStyleLandscape;
    }
    return styles.headerStylePortrait;
  }

  componentDidMount() {
    this.props.startTraining();
  }

  render() {
    return (
      <View
        style={styles.container}
        onLayout={event => {
          this.props.resizeScreen(event);
          this.props.toggleMenu(false);
        }}
      >
        {this.props.image.trainingImages.processing ? (
          <View style={styles.evaluating}>
            <ActivityIndicator size="large" color="#0000ff" animating={true} />
            <Svg
              width={this.props.screen.dimensions.width}
              height={this.props.screen.dimensions.height / 3}
            >
              <Text
                fill="black"
                stroke="black"
                fontSize="30"
                x={this.props.screen.dimensions.width / 2}
                y={this.props.screen.dimensions.height / 6}
                textAnchor="middle"
              >
                Processing
              </Text>
            </Svg>
          </View>
        ) : (
          <RNCamera
            ref={ref => (CTCamera = ref)}
            captureAudio={false}
            style={styles.camera}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.off}
            permissionDialogTitle={"Permission to use camera"}
            permissionDialogMessage={
              "We need your permission to use your camera phone"
            }
          >
            <View style={styles.cameraContainer}>
              <Header
                containerStyle={this.getHeaderStyle()}
                leftComponent={{
                  icon: "stop",
                  color: "#fff",
                  onPress: this.props.stopTraining,
                  underlayColor: "#64b5f6"
                }}
                centerComponent={{
                  text: "Class Training",
                  style: { color: "#fff" }
                }}
                rightComponent={{
                  icon: "menu",
                  color: "#fff",
                  onPress: () => this.props.toggleMenu(true),
                  underlayColor: "#64b5f6"
                }}
              />
              <TouchableOpacity
                style={styles.button}
                onPressIn={this.props.startBurstTrainingImage}
                onPressOut={this.props.stopBurstTrainingImage}
              />
            </View>
          </RNCamera>
        )}
      </View>
    );
  }
}

ClassTraining.propTypes = {
  image: PropTypes.object,
  screen: PropTypes.object,
  takeImage: PropTypes.func,
  resizeScreen: PropTypes.func,
  toggleMenu: PropTypes.func,
  startTraining: PropTypes.func,
  startBurstTrainingImage: PropTypes.func,
  stopBurstTrainingImage: PropTypes.func,
  stopTraining: PropTypes.func
};

const mapStateToProps = state => ({
  screen: state.screen,
  image: state.image
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ ...screenActions, ...imageActions }, dispatch);
};

export { CTCamera };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClassTraining);
