/** @format */

import React, { Component } from "react";
import { StyleSheet, ScrollView, View } from "react-native";
import { Header, ListItem } from "react-native-elements";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { classActions } from "../../../state/ducks/class";
import { screenActions } from "../../../state/ducks/screen";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white"
  },
  listContainer: {
    marginTop: 0
  },
  headerStylePortrait: {
    paddingTop: 0,
    width: "100%",
    height: "7.5%",
    borderBottomColor: "transparent"
  },
  headerStyleLandscape: {
    paddingTop: 0,
    width: "100%",
    height: "12%",
    borderBottomColor: "transparent"
  }
});

class ClassSelector extends Component {
  getHeaderStyle() {
    if (
      this.props.screen.dimensions.width > this.props.screen.dimensions.height
    ) {
      return styles.headerStyleLandscape;
    }
    return styles.headerStylePortrait;
  }

  render() {
    return (
      <View
        style={styles.container}
        onLayout={event => {
          this.props.resizeScreen(event);
          this.props.toggleMenu(false);
        }}
      >
        <Header
          containerStyle={this.getHeaderStyle()}
          centerComponent={{
            text: "Select classes",
            style: { color: "#fff" }
          }}
          rightComponent={{
            icon: "menu",
            color: "#fff",
            onPress: () => this.props.toggleMenu(true),
            underlayColor: "#64b5f6"
          }}
        />
        <ScrollView style={styles.listContainer}>
          {this.props.class.map((classObject, classIndex) => (
            <ListItem
              chevron={false}
              key={classIndex}
              title={classObject.name}
              leftIcon={{
                name: classObject.active ? "done" : "not-interested",
                color: classObject.active ? "#008200" : "#d0021b"
              }}
              onPress={() => {
                this.props.toggleClass(classObject.name);
              }}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}

ClassSelector.propTypes = {
  screen: PropTypes.object,
  "class": PropTypes.array,
  toggleClass: PropTypes.func,
  resizeScreen: PropTypes.func,
  toggleMenu: PropTypes.func
};

const mapStateToProps = state => ({
  screen: state.screen,
  "class": state.class
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ ...screenActions, ...classActions }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClassSelector);
