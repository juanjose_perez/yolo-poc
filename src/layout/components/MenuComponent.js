/** @format */

import React, { Component } from "react";
import { StyleSheet, ScrollView, View } from "react-native";
import { ListItem, Icon, Text } from "react-native-elements";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { screenActions } from "../../state/ducks/screen";

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    flex: 1
  },
  menuToolbar: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  },
  menuToolbarText: {
    marginLeft: 10
  },
  listContainer: {
    width: "100%",
    marginTop: 5
  },
  listItemStylePortrait: {
    width: "100%",
    borderBottomColor: "transparent"
  },
  listItemStyleLandscape: {
    width: "100%",
    borderBottomColor: "transparent"
  }
});

class MenuComponent extends Component {
  getListItemStyle() {
    if (
      this.props.screen.dimensions.width > this.props.screen.dimensions.height
    ) {
      return styles.listItemStyleLandscape;
    }
    return styles.listItemStylePortrait;
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.menuToolbar}>
          <Text h4 style={styles.menuToolbarText}>
            {"Navigation menu"}
          </Text>
          <Icon name={"close"} onPress={() => this.props.toggleMenu(false)} />
        </View>
        <ScrollView style={styles.listContainer}>
          <ListItem
            style={this.getListItemStyle()}
            key={1}
            title="Image Classification"
            leftIcon={{
              name: "image"
            }}
            onPress={() => {
              setTimeout(() => this.props.push("/"), 400);
              this.props.toggleMenu(false);
            }}
          />
          <ListItem
            style={this.getListItemStyle()}
            key={2}
            title="Class Training"
            leftIcon={{
              name: "burst-mode"
            }}
            onPress={() => {
              setTimeout(() => this.props.push("/training"), 400);
              this.props.toggleMenu(false);
            }}
          />
          <ListItem
            style={this.getListItemStyle()}
            key={3}
            title="Class Selector"
            leftIcon={{
              name: "class"
            }}
            onPress={() => {
              setTimeout(() => this.props.push("/classSelector"), 400);
              this.props.toggleMenu(false);
            }}
          />
          <ListItem
            style={this.getListItemStyle()}
            key={4}
            title="Classifier Selector"
            leftIcon={{
              name: "flag"
            }}
            onPress={() => {
              setTimeout(() => this.props.push("/classifierSelector"), 400);
              this.props.toggleMenu(false);
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

MenuComponent.propTypes = {
  screen: PropTypes.object,
  toggleMenu: PropTypes.func,
  push: PropTypes.func
};

const mapStateToProps = state => ({
  screen: state.screen
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ ...screenActions, push }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuComponent);
