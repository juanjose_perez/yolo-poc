package com.yolopoc;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Canvas;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;

import org.tensorflow.lite.Interpreter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

class TensorFlowYoloDetector {
  private static final String YOLO_MODEL_FILE = "tiny-yolo-voc.tflite";
  private static final String YOLO_LABELS_FILE = "tiny-yolo-voc-labels.txt";
  private static final int YOLO_INPUT_SIZE = 416;
  private static final int YOLO_BLOCK_SIZE = 32;

  private static final int NUM_CLASSES = 20;
  private static final int NUM_BOXES_PER_BLOCK = 5;
  private static final int NUM_BYTES_PER_CHANNEL = 4;

  private static final double[] ANCHORS = {
    1.08, 1.19,
    3.42, 4.41,
    6.63, 11.38,
    9.42, 5.11,
    16.62, 10.52
  };

  private String[] labels;

  private int[] intValues;

  private int inputSize;
  private int blockSize;

  private Interpreter tfLite;
  private ByteBuffer imgData;

  // Only return this many results.
  // private boolean isModelQuantized;
  // Float model
  // Number of threads in the java app
  // private static final int NUM_THREADS = 4;
  // Config values.
  // Pre-allocated buffers.

  public TensorFlowYoloDetector(ReactContext context) {
    this.inputSize = YOLO_INPUT_SIZE;
    this.blockSize = YOLO_BLOCK_SIZE;

    Interpreter.Options options = new Interpreter.Options();
    options.setUseNNAPI(false);
    options.setAllowFp16PrecisionForFp32(false);
    options.setNumThreads(4);
    try {
      this.labels = loadLabelsFile(context.getAssets());
      this.tfLite = new Interpreter(loadModelFile(context.getAssets()), options);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    // Pre-allocate buffers.
    int numBytesPerChannel;

    numBytesPerChannel = NUM_BYTES_PER_CHANNEL; // Floating point

    this.imgData = ByteBuffer.allocateDirect(this.inputSize * this.inputSize * 3 * numBytesPerChannel);
    this.imgData.order(ByteOrder.nativeOrder());
    this.intValues = new int[this.inputSize * this.inputSize];

    //d.tfLite.setNumThreads(NUM_THREADS);
  }

  private static MappedByteBuffer loadModelFile(AssetManager assets)
          throws IOException {
    AssetFileDescriptor fileDescriptor = assets.openFd(YOLO_MODEL_FILE);
    FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
    FileChannel fileChannel = inputStream.getChannel();
    long startOffset = fileDescriptor.getStartOffset();
    long declaredLength = fileDescriptor.getDeclaredLength();
    return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
  }

  private static String[] loadLabelsFile(AssetManager assets) throws IOException {
    BufferedReader reader = new BufferedReader(
            new InputStreamReader(assets.open(YOLO_LABELS_FILE)));
    ArrayList<String> stringList = new ArrayList<>();
    String mLine;
    while ((mLine = reader.readLine()) != null) {
      stringList.add(mLine);
    }

    return stringList.toArray(new String[stringList.size()]);
  }

  public WritableArray recognizeImage(final Bitmap bitmapRaw) {
    // Log this method so that it can be analyzed with systrace.
    // Preprocess the image data from 0-255 int to normalized float based
    // on the provided parameters.
    intValues = new int[inputSize * inputSize];

    Bitmap bitmap = Bitmap.createBitmap(inputSize, inputSize, Bitmap.Config.ARGB_8888);


    Matrix matrix = getTransformationMatrix(
            bitmapRaw.getWidth(), bitmapRaw.getHeight(),
            inputSize, inputSize, false);

    final Canvas canvas = new Canvas(bitmap);

    // transform the image to a inputSize * inputSize bitmap from its original size using a transformation matrix
    canvas.drawBitmap(bitmapRaw, matrix, null);
    bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

    imgData.rewind();

    // We normalize the image matrix from 0-FFFFFF RGB int to 0-1
    for (int i = 0; i < inputSize; ++i) {
      for (int j = 0; j < inputSize; ++j) {
        int pixelValue = intValues[i * inputSize + j];
          // Float model
          imgData.putFloat(((pixelValue >> 16) & 0xFF) / 255.0f);
          imgData.putFloat(((pixelValue >> 8) & 0xFF) / 255.0f);
          imgData.putFloat((pixelValue & 0xFF) / 255.0f);

      }
    }

    final int gridWidth = bitmap.getWidth() / blockSize;
    final int gridHeight = bitmap.getHeight() / blockSize;
    final float[][][][] output = new float[1][gridHeight][gridWidth][(NUM_CLASSES + 5) * NUM_BOXES_PER_BLOCK];

    tfLite.run(imgData, output);

    int ind = 0;


    WritableArray results = Arguments.createArray();

    // The output is a tensor composed  of:
    // Dimension 1 (y): Height (per block)
    // Dimension 2 (x): Width (per block)
    // Dimension 3 (b): An array of: [center of most confidence (x dimension, y dimension), box height, box width, max confidence, (confidences per (anchor * class))]
    for (int y = 0; y < gridHeight; ++y) {
      for (int x = 0; x < gridWidth; ++x) {
        for (int b = 0; b < NUM_BOXES_PER_BLOCK; ++b) {
          final int offset = (NUM_CLASSES + 5) * b;

          final float confidence = expit(output[0][y][x][offset + 4]);

          int detectedClass = -1;
          float maxClass = 0;

          final float[] classes = new float[NUM_CLASSES];
          for (int c = 0; c < NUM_CLASSES; ++c) {
            classes[c] = output[0][y][x][offset + 5 + c];
          }

          // we normalize the classes confidence vector
          softmax(classes);

          // calculate the best class for the current block
          for (int c = 0; c < NUM_CLASSES; ++c) {
            if (classes[c] > maxClass) {
              detectedClass = c;
              maxClass = classes[c];
            }
          }

          // calculate the absolute confidence in the max class
          final float confidenceInClass = maxClass * confidence;
          if (confidenceInClass > 0.2) {
            final float xPos = (x + expit(output[0][y][x][offset + 0])) * blockSize;
            final float yPos = (y + expit(output[0][y][x][offset + 1])) * blockSize;

            final float w = (float) (Math.exp(output[0][y][x][offset + 2]) * ANCHORS[2 * b + 0]) * blockSize;
            final float h = (float) (Math.exp(output[0][y][x][offset + 3]) * ANCHORS[2 * b + 1]) * blockSize;

            WritableMap rect = Arguments.createMap();
            rect.putDouble("x", (double)Math.max(0, xPos - w / 2));
            rect.putDouble("y", (double)Math.max(0, yPos - h / 2));
            rect.putDouble("w", w);
            rect.putDouble("h", h);

            WritableMap result = Arguments.createMap();
            result.putMap("rect", rect);
            result.putDouble("confidenceInClass", confidenceInClass);
            if (detectedClass > -1) {
              result.putString("detectedClass", this.labels[detectedClass]);
              result.putInt("detectedClassIndex", detectedClass);
            } else {
              result.putString("detectedClass", null);
              result.putInt("detectedClassIndex", 0);
            }


            results.pushMap(result);
          }
        }
      }
    }

    return results;
  }

  private float expit(final float x) {
    return (float) (1. / (1. + Math.exp(-x)));
  }

  private void softmax(final float[] vals) {
    float max = Float.NEGATIVE_INFINITY;
    for (final float val : vals) {
      max = Math.max(max, val);
    }
    float sum = 0.0f;
    for (int i = 0; i < vals.length; ++i) {
      vals[i] = (float) Math.exp(vals[i] - max);
      sum += vals[i];
    }
    for (int i = 0; i < vals.length; ++i) {
      vals[i] = vals[i] / sum;
    }
  }

  public static Matrix getTransformationMatrix(final int srcWidth,
                                               final int srcHeight,
                                               final int dstWidth,
                                               final int dstHeight,
                                               final boolean maintainAspectRatio)
  {
      final Matrix matrix = new Matrix();

      if (srcWidth != dstWidth || srcHeight != dstHeight) {
          final float scaleFactorX = dstWidth / (float) srcWidth;
          final float scaleFactorY = dstHeight / (float) srcHeight;

          if (maintainAspectRatio) {
            final float scaleFactor = Math.max(scaleFactorX, scaleFactorY);
            matrix.postScale(scaleFactor, scaleFactor);
          } else {
            matrix.postScale(scaleFactorX, scaleFactorY);
          }
      }

      matrix.invert(new Matrix());
      return matrix;
  }


}
